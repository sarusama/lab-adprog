package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class StiffDough implements Dough {
    public String toString() {
        return "Stiff style extra Stiff dough";
    }
}
