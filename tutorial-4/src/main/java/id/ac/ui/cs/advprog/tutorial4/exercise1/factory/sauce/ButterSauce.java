package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class ButterSauce implements Sauce {
    public String toString() {
        return "Butter Sauce";
    }
}
